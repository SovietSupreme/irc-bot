//
// by Joseph E. Sloan joe.sloan@outlook.com
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define BUF_SIZE 9000
#define PORT 6667

int sock;

//
// substring
//
char * substr (char * text,int posStart,int posEnd) {
	if((posStart >= 0 && posEnd >=0)) {
		char * retVal = malloc(posEnd + 1);
		memcpy(retVal,&text[posStart],posEnd);
		retVal[posEnd] = '\0';
		return retVal;
	} else {
		printf("substr: params are incorrect.\n");
	}

	return NULL;
}


//
//
//
int find (char* text,const char* token,int occurs) {
	char * slice;
	int occurances = 0;
	int offset = strlen(token);
	int posStop = strlen(text);
	//printf("posStop: %d\n",posStop);
	for(int i = 0;i<(posStop);i++) {
		if((slice = substr(text,i,offset)) != NULL) {
	//		printf("slice: %s %d %d %d\n",slice,i,offset,occurances);
			if(strcmp(slice,token) == 0 ) {
				if(!occurs) {
					free(slice);
					return i;
				} else {
					occurances++;
				}
			}
			free(slice);
		}
	}
	if(occurs) {
		size_t dLen = strlen(token);
		size_t tLen = strlen(text);
		char* tmp = substr(text,(tLen - dLen),dLen);
		if(strcmp(tmp,token) == 0) {
			free(tmp);
			return occurances;
		} else {
			free(tmp);
			return (occurances + 1);
		}
	} else {
		return -1;
	}
}

//
// Chops up a string into substrings by a given character delimiter
//
char** getTokens(char* text, const char* delimiter) {
	size_t dLen = strlen(delimiter);
	size_t occurs = find(text,delimiter,1);
	char** tokens = malloc(occurs * sizeof(char*));
	int sPos = 0;
	int pos = 0;
	int incr = 0;
	while((pos = find(text,delimiter,0)) != -1) {
		char * token = substr(text,0,pos);
		*(tokens + incr) = strdup(token);
		sPos = (pos + dLen);
		text += sPos;
		free(token);
		incr++;
	}
	
		
	if(strlen(text) > 0) {
		char * token = substr(text,0,strlen(text));
		*(tokens + incr) = strdup(token);
		free(token);
	}

	return tokens;

}

//
// joins 2 strings together by given character
//
char* join(char* array[],const char* joinBy, size_t size) {
	char** tmpP = array;
	char * line = NULL;
	int count = 0;
	int bytes = 0;
	int n = size;
	int x = 0;
	int y = 0;
	int jbl = strlen(joinBy);

	for(int z = 0;z<size;z++) {
		bytes += strlen(array[z]);
	}

	count = bytes + (n * jbl);

	line = malloc((count + 1));
	line[0] = '\0';

	for(int z=0;z<size;z++) {
		strcat(line,array[z]);
		strcat(line,joinBy);
	}

	line[count] = '\0';

	char * tmp = substr(line,0,(strlen(line) - strlen(joinBy)));
	free(line);
	return tmp;
}

//
// parses response from server 
//
int parseMessage(char** tokens, size_t size) {
	if(size > 1) {
		//
		// MOTD
		//
		if(strcmp(*(tokens + 1),"372") == 0) {
			assert(size >= 3);
			char* line = join((tokens + 3)," ",size - 3);
			printf("%s\n",line);
			free(line);	
		//
		// WELCOME
		//
		} else if(strcmp(*(tokens + 1),"001") == 0) {
			assert(size >= 3);
			char* line = join((tokens + 3)," ",size - 3);
			printf("[WELCOME]: %s\n",line);
			free(line);	
		} else {
			char* line = join(tokens," ",size);
			printf("[log]: %s\n",line);
			free(line);
		}
	}
	return 0;
}


int main(int argc,char** argv) {

/*
	char * manowar = "ManOwar Born to fight forever more!\r\nblah blah blah\r\ndasdjagdjagjdg\r\ndsfbsjkfhskhf\r\n";
	char buf[256];
	sprintf(buf,"%s",manowar);	
	char** tokens = getTokens(buf,"\r\n");
	char * line = join(tokens," -- ");
	int arSize = find(buf,"\r\n",1);
	printf("line: %s\n",line);
	free(line);
	int index;
	for(index=0;index<arSize;index++) {
		printf("token: %s\n",*(tokens + index));
		free(*(tokens + index));
	}
	free(tokens);

	return 0;
*/
	struct sockaddr_in serv_addr;
	int nBytes;
	char buffer[BUF_SIZE];

	memset(&sock,'0',sizeof(sock));
	memset(&serv_addr,'0',sizeof(serv_addr));
	//memset(buffer,'0',BUF_SIZE);	
		
	struct hostent * record = gethostbyname("chat.freenode.net");
	if(record == NULL) {
		printf("Host unavailable.");
		return -1;
	}

	struct in_addr * address = (struct in_addr *)record->h_addr;
	char * ip_address = inet_ntoa(*address);

	printf("Resolved to %s\n",ip_address);

	if((sock = socket(AF_INET,SOCK_STREAM,0)) < 0) {
		perror("Socket creation failure");
		return -1;
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	if(inet_pton(AF_INET,ip_address,&serv_addr.sin_addr) <= 0) {
		perror("Issue with ip address");
		return -1;
	}

	printf("Connectting to %s.....\n",ip_address);

	if(connect(sock,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
		perror("Unable to connect");
		return -1;
	}

	printf("Connection established\n");

	const char * userString = "USER sloanj rootyou.org chat.freenode.net : Joseph Sloan\r\n";
	const char * nickString = "NICK Lightwar6\r\n";

	send(sock,userString,strlen(userString),0);
	send(sock,nickString,strlen(nickString),0);

	for(;;) {
		if((nBytes = recv(sock,buffer,BUF_SIZE,0)) <= 0) {
			if(errno != EWOULDBLOCK) {
				perror("Disconnected");
				return -1;
			}
		} else {
			char* message = malloc((nBytes + 1));
			memcpy(message,buffer,nBytes);
			message[nBytes] = '\0';
			char** lines = getTokens(message,"\r\n");
			int num_lines = find(message,"\r\n",1);
			//printf("num_lines: %d\n",num_lines);
			free(message);
			//return 0;
			int i;
			for (i=0;i<num_lines;i++) {
				char** tokens = getTokens(*(lines + i)," ");
				int num_tokens = find(lines[i]," ",1);
				
				char* who;// = malloc(strlen((char*)*(tokens + 0)));
				who = strdup((char*)*(tokens +0));
				
				if(strcmp(who,"PING") == 0) {
					char** response = getTokens(*(lines + i),":");
					size_t pongSize = strlen(response[1]) + 7;
					char* pongReply = malloc((pongSize + 1));
					sprintf(pongReply,"PONG %s\r\n",response[1]);
					pongReply[pongSize] = '\0';
					printf("%s",pongReply);
					send(sock,pongReply,strlen(pongReply),0);
					free(pongReply);
					int size = find(*(lines + i),":",1);
					for(int y=0;y<size;y++) {
						free(*(response + y));
					}
					free(response);
				} else {
					parseMessage(tokens,num_tokens);
				}
				
				free(who);
				int n;
				free(*(lines + i));
				for(n = 0;n<num_tokens;n++) {
					free(*(tokens + n));
				}
				free(tokens);
			}
			free(lines);
		}
	}

		
	/*	
	char text[] = "Other Bands play..\n\rManOWar kill!";
	int pos = 0;

	char** tokens;

	char* result = join(argv," ");

	printf("%s\n",result);

	free(result);
*/
	//if(argc > 0) {
	//	for(int i=0;i<argc;i++) {
	//		printf("%s\n",*(argv + i));
	//	}
	//}
	/*
	if((pos = find(text,' ')) != -1) {
		printf("found %c as pos %d\n","a",pos);
		tokens = split(text,'\n');
		
		for(int i=0;*(tokens + i); i++) {
			printf("%s\n",*(tokens + i));
			free(*(tokens + i));
		}	
		printf("\n");
		free(tokens);
	}
	*/
	return 0;
}
